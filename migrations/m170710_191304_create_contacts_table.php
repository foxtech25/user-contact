<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contacts`.
 */
class m170710_191304_create_contacts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contacts', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string()->notNull(),
            'created_at' => $this->timestamp()
        ]);

        $this->insert('contacts', [
            'name' => 'facebook',
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('contacts');
    }
}
