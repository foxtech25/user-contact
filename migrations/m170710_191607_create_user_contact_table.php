<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_contact`.
 */
class m170710_191607_create_user_contact_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_contact', [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer()->notNull(),
            'contact_id' => $this->integer()->notNull(),
            'value'      => $this->string(200)->notNull(),
            'is_history' => $this->boolean()->defaultValue(0)->notNull(),
            'created_at' => $this->timestamp()
        ]);

        // to user table
        $this->createIndex(
            'idx-user_contact-user_id',
            'user_contact',
            'user_id'
        );

        $this->addForeignKey(
            'fk-users-user_id',
            'user_contact',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );

        // to contacts table
        $this->createIndex(
            'idx-user_contact-contact_id',
            'user_contact',
            'contact_id'
        );

        $this->addForeignKey(
            'fk-user_contact-contact_id',
            'user_contact',
            'contact_id',
            'contacts',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_contact');
    }
}