<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170710_185345_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string()->notNull(),
            'created_at' => $this->timestamp()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
