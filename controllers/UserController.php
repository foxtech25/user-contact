<?php

namespace app\controllers;

use app\models\Contact;
use app\models\User;
use app\models\UserContact;
use Yii;
use yii\rest\ActiveController;

/**
 * Class UserController
 * @package app\controllers
 */
class UserController extends ActiveController
{
    /**
     * @var string
     */
    public $modelClass = 'app\models\User';

    /**
     *
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    /**
     * @apiGroup           Users
     * @apiName            ListUsersWithContacts
     * @api                {get} /users List users with contacts
     * @apiVersion         1.0.0
     * @apiPermission      none
     * @apiDescription     List Users With Contacts
     *
     * @apiHeader          Accept application/json (required)
     *
     * @apiSuccessExample  {json} Success-example:
    {
        "22": {
        "user_name": "NAME111111",
            "now": [
                {
                    "contact_name": "sdfs",
                    "value": "sdfdsf"
                },
                {
                    "contact_name": "234234234",
                    "value": "sfdege"
                }
            ],
            "old": {
            "2015": [
                    {
                        "contact_name": "sdfs",
                        "value": "sdfgsdf"
                    }
                ],
                "2017": [
                    {
                        "contact_name": "sdfs",
                        "value": "ddddd"
                    },
                    {
                        "contact_name": "234234234",
                        "value": "sssssssss"
                    },
                    {
                        "contact_name": "234234234",
                        "value": "sdfsd"
                    }
                ]
            }
        },
        "23": {
        "user_name": "gug",
            "now": [
                {
                    "contact_name": "234234234",
                    "value": "fff"
                },
                {
                    "contact_name": "sdfs",
                    "value": "fff"
                }
            ]
        }
    }
    */
    public function actionIndex()
    {
        $userContact = [];

        foreach (User::find()
                     ->with('userContactsNew')
                     ->with('userContactsOld')
                     ->all()
                 as $user
        ) {
            $userContact[$user->id]['user_name'] = $user['name'];

            foreach ($user->userContactsNew as $contact) {
                $userContact[$contact->user->id]
                ['now']
                [] = [
                    'contact_name' => $contact->contact->name,
                    'value'        => $contact->value
                ];
            }

            foreach ($user->userContactsOld as $contact) {
                $userContact[$contact->user->id]
                ['old']
                [explode('-', $contact->created_at)[0]]
                [] = [
                    'contact_name' => $contact->contact->name,
                    'value'        => $contact->value
                ];
            }
        }

        return $userContact;
    }

    /**
     * @apiGroup           Users
     * @apiName            GetUserWithContacts
     * @api                {get} /users/{id} Get user with contacts
     * @apiVersion         1.0.0
     * @apiPermission      none
     * @apiDescription     Get User With Contacts
     *
     * @apiHeader          Accept application/json (required)
     *
     * @apiParam           {integer} id (required) User id (param in url)
     *
     * @apiSuccessExample  {json} Success-example:
    {
        "user": {
            "id": 22,
            "name": "NAME111111",
            "created_at": "2017-07-12 23:18:19"
        },
        "contact_now": [
            {
                "contact_name": "sdfs",
                "value": "sdfdsf"
            },
            {
                "contact_name": "234234234",
                "value": "sfdege"
            }
        ],
        "contact_old": {
        "2015": [
                {
                    "contact_name": "sdfs",
                    "value": "sdfgsdf"
                }
            ],
            "2017": [
                {
                    "contact_name": "sdfs",
                    "value": "ddddd"
                },
                {
                    "contact_name": "234234234",
                    "value": "sssssssss"
                },
                {
                    "contact_name": "234234234",
                    "value": "sdfsd"
                }
            ]
        }
    }
    */
    public function actionView()
    {
        $request = Yii::$app->request->get();
        $userContact = [];

        $user = User::findOne($request['id']);

        foreach ($user->userContactsNew as $contact) {
            $userContact['now']
            [] = [
                'contact_name' => $contact->contact->name,
                'value'        => $contact->value
            ];
        }

        foreach ($user->userContactsOld as $contact) {
            $userContact['old']
            [explode('-', $contact->created_at)[0]]
            [] = [
                'contact_name' => $contact->contact->name,
                'value'        => $contact->value
            ];
        }

        return [
            'user'        => $user,
            'contact_now' => $userContact['now'] ?? null,
            'contact_old' => $userContact['old'] ?? null,
        ];
    }

    /**
     * @apiGroup           Users
     * @apiName            CreateUserWithContacts
     * @api                {post} /users Create user with contacts
     * @apiVersion         1.0.0
     * @apiPermission      none
     * @apiDescription     Create User With Contacts
     *
     * @apiHeader          Accept application/json (required)
     *
     * @apiParam           {string} name             (required) User name
     * @apiParam           {array}  contacts         (required) Array contacts
     * @apiParam           {string} contacts.*.name  (required) Contact name
     * @apiParam           {string} contacts.*.value (required) Contact value
     *
     * @apiParamExample    {json} Param-example:
    {
        "name":"gug",
        "contacts":[
            {
                "name": "234234234",
                "value":"fff"
            },
            {
                "name": "sdfs",
                "value":"fff"
            }
        ]
    }
     *
     * @apiSuccessExample  {json} Success-example:
    {
        "success": true
    }
     */
    public function actionCreate()
    {
        $request = Yii::$app->request->post();
        $transaction = Yii::$app->db->beginTransaction();

        try {
            if (!isset($request['user_id'])) {
                $user = new User();
                $user->load($request);
                $user->name = $request['name'];
                $user->save();
            } else {
                $user = User::findOne($request['user_id']);

                if (!$user) {
                    throw new \Exception('User not found');
                }
            }

            foreach ($request['contacts'] as $requestContact) {
                $contact = new Contact();
                $contact->load($request);
                $contact->name = $requestContact['name'];
                $contact->save();

                $userContact = new UserContact();
                $userContact->load($request);
                $userContact->user_id = $user->id;
                $userContact->contact_id = $contact->id;
                $userContact->value = $requestContact['value'];
                $userContact->save();
            }

            $transaction->commit();

            return [
                'success' => true
            ];

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return [
            'success' => false
        ];
    }

    /**
     * @apiGroup           Users
     * @apiName            UpdateUserWithContacts
     * @api                {put} /users/{id} Update user with contacts
     * @apiVersion         1.0.0
     * @apiPermission      none
     * @apiDescription     Update User With Contacts
     *
     * @apiHeader          Accept application/json (required)
     *
     * @apiParam           {integer} id               (required) User id (param in url)
     * @apiParam           {string}  name             (optional) User name
     * @apiParam           {array}   contacts         (optional) Array contacts
     * @apiParam           {string}  contacts.*.name  (optional) Contact name
     * @apiParam           {string}  contacts.*.value (optional) Contact value
     *
     * @apiParamExample    {json} Param-example:
    {
        "name":"gug",
        "contacts":[
            {
                "name": "234234234",
                "value":"fff"
            },
            {
                "name": "sdfs",
                "value":"fff"
            }
        ]
    }
     *
     * @apiSuccessExample  {json} Success-example:
    {
        "success": true
    }
     */
    public function actionUpdate()
    {
        $request = Yii::$app->request->getBodyParams();
        $transaction = Yii::$app->db->beginTransaction();

        try {

            $user = User::findOne(
                Yii::$app->request->get()['id']
            );

            if (!$user) {
                throw new \Exception('User not found');
            }

            if (isset($request['name'])) {
                $user->name = $request['name'];
                $user->save();
            }

            foreach ($request["contacts"] as $contact) {
                $userContact = UserContact::findOne([
                    'user_id' => $user['id'],
                    'contact_id' => $contact['id'],
                ]);

                if (!$userContact) {
                    throw new \Exception('Contact id ' . $contact['id'] . ' in this user not fount');
                }

                $userContact['value'] = $contact['value'];
                $userContact->save();
            }

            $transaction->commit();

            return [
                'success' => true
            ];

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return [
            'success' => false
        ];
    }

    /**
     * @apiGroup           Users
     * @apiName            DeleteUserWithContacts
     * @api                {delete} /users/{id} Delete user with contacts
     * @apiVersion         1.0.0
     * @apiPermission      none
     * @apiDescription     Delete User With Contacts
     *
     * @apiHeader          Accept application/json (required)
     *
     * @apiParam           {integer} id (required) User id (param in url)
     *
     *
     * @apiSuccessExample  {json} Success-example:
    {
        "success": true
    }
     */
    public function actionDelete()
    {
        $userId = Yii::$app->request->get()['id'];

        $user = User::findOne([
            'id' => $userId
        ]);

        if (!$user) {
            throw new \Exception('User not found');
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {

            $userContact = UserContact::findAll([
                'user_id' => $userId
            ]);

            foreach ($userContact as $contact) {
                Contact::deleteAll([
                    'id' => $contact['contact_id']
                ]);
            }

            $user->delete();

            $transaction->commit();

            return [
                'success' => true
            ];

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return [
            'success' => false
        ];
    }

    /**
     * @apiGroup           Contacts
     * @apiName            DeleteContacts
     * @api                {delete} /contacts Delete contacts
     * @apiVersion         1.0.0
     * @apiPermission      none
     * @apiDescription     Delete Contacts
     *
     * @apiHeader          Accept application/json (required)
     *
     * @apiParam           {array} contacts (required) Array contacts ids
     *
     * @apiParamExample    {json} Param-example:
    {
        "contacts": [1,2,3,4]
    }
     *
     * @apiSuccessExample  {json} Success-example:
    {
        "success": true
    }
     */
    public function actionContactDelete()
    {
        $request = Yii::$app->request->getBodyParams();
        $transaction = Yii::$app->db->beginTransaction();

        try {

            foreach ($request['contacts'] as $contactId) {
                Contact::deleteAll([
                    'id' => $contactId
                ]);
            }

            $transaction->commit();

            return [
                'success' => true
            ];

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return [
            'success' => false
        ];
    }

    /**
     * @apiGroup           Users
     * @apiName            MakeContactsHistory
     * @api                {delete} /contacts/soft Make contacts history
     * @apiVersion         1.0.0
     * @apiPermission      none
     * @apiDescription     Make contacts history
     *
     * @apiHeader          Accept application/json (required)
     *
     * @apiParam           {integer} user_id  (required) User id
     * @apiParam           {array}   contacts (required) Array contacts ids
     *
     * @apiParamExample    {json} Param-example:
    {
        "user_id":22,
        "contacts":[6,7]
    }
     *
     * @apiSuccessExample  {json} Success-example:
    {
    "success": true
    }
     */
    public function actionContactMakeHistory()
    {
        $request = Yii::$app->request->getBodyParams();

        $user = User::findOne([
            'id' => $request['user_id']
        ]);

        if (!$user) {
            throw new \Exception('User not found');
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {

            foreach ($request['contacts'] as $contactId) {
                $userContact = UserContact::findOne([
                    'user_id' => $user['id'],
                    'contact_id' => $contactId
                ]);

                if ($userContact->is_history == false) {
                    $userContact->is_history = true;
                    $userContact->save();
                }
            }

            $transaction->commit();

            return [
                'success' => true
            ];

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return [
            'success' => false
        ];
    }

    /**
     * @apiGroup           Users
     * @apiName            GetUserContactsByDate
     * @api                {get} /users/{id}/date/{date} Get user contact by dage
     * @apiVersion         1.0.0
     * @apiPermission      none
     * @apiDescription     Get User Contacts By Date
     *
     * @apiHeader          Accept application/json (required)
     *
     * @apiParam           {integer} user_id  (required) User id (param in url)
     * @apiParam           {string}   date (required) Date (format yyyy-MM-DD) (param in url)
     *
     * @apiSuccessExample  {json} Success-example:
    {
        "user": {
            "id": 22,
            "name": "NAME111111",
            "created_at": "2017-07-12 23:18:19"
        },
        "contact_now": {
            "2016": [
                {
                    "contact_name": "sdfs",
                    "value": "sdfdsf"
                },
                {
                    "contact_name": "234234234",
                    "value": "sfdege"
                }
            ]
        },
        "contact_old": {
            "2015": [
                {
                    "contact_name": "sdfs",
                    "value": "sdfgsdf"
                }
            ],
            "2017": [
                {
                    "contact_name": "sdfs",
                    "value": "ddddd"
                },
                {
                    "contact_name": "234234234",
                    "value": "sssssssss"
                },
                {
                    "contact_name": "234234234",
                    "value": "sdfsd"
                }
            ]
        }
    }
     */
    public function actionGetByDate()
    {
        $request = Yii::$app->request->get();

        $user = User::findOne([
            'id' => $request['id']
        ]);

        if (!$user) {
            throw new \Exception('User not found');
        }

        $request['date'] = Yii::$app->formatter->asDate($request['date'], 'yyyy-MM-dd');

        if (!$request['date']) {
            throw new \Exception('Data format incorect');
        }

        $userContact = [];

        foreach ($user->userContactsNew as $contact)
        {
            if ($request['date'] <= $contact->created_at)
            {
                $userContact['now']
                [explode('-', $contact->created_at)[0]]
                [] = [
                    'contact_name' => $contact->contact->name,
                    'value' => $contact->value
                ];
            }
        }

        foreach ($user->userContactsOld as $contact)
        {
            if ($request['date'] <= $contact->created_at)
            {
                $userContact['old']
                [explode('-', $contact->created_at)[0]]
                [] = [
                    'contact_name' => $contact->contact->name,
                    'value' => $contact->value
                ];
            }
        }

        return [
            'user'        => $user,
            'contact_now' => $userContact['now'] ?? null,
            'contact_old' => $userContact['old'] ?? null,
        ];
    }
}