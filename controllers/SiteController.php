<?php

namespace app\controllers;

use yii\rest\Controller;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /*
     *
     */
    public function actionIndex()
    {
        return [
            'pages' => [
                'PUT,PATCH users/<id>',
                'DELETE users/<id>',
                'DELETE contacts',
                'DELETE contacts/soft',
                'GET,HEAD users/<id>',
                'POST users',
                'GET,HEAD users',

            ]
        ];
    }
}