<?php
namespace tests\models;
use app\models\UserContact;

class UserContactTest extends \Codeception\Test\Unit
{
    public function testFindUserContactByName()
    {
        expect_that($user = UserContact::findOne(['user_id' => '22']));
        expect_not(UserContact::findOne(['user_id' => 'not-admin']));
    }
}
