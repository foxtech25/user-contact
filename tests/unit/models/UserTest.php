<?php
namespace tests\models;
use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    public function testFindUserByName()
    {
        expect_that($user = User::findOne(['name' => 'admin']));
        expect_not(User::findOne(['name' => 'not-admin']));
    }
}
