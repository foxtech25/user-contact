<?php
namespace tests\models;
use app\models\Contact;

class ContactTest extends \Codeception\Test\Unit
{
    public function testFindContactByName()
    {
        expect_that($user = Contact::findOne(['name' => '234234234']));
        expect_not(Contact::findOne(['name' => 'not-admin']));
    }
}
