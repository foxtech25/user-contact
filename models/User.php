<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $name
 * @property string $created_at
 *
 * @property UserContact[] $userContacts
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserContactsNew()
    {
        return $this->hasMany(UserContact::className(), ['user_id' => 'id'])
            ->where(['is_history' => 0])
            ->orderBy('created_at DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserContactsOld()
    {
        return $this->hasMany(UserContact::className(), ['user_id' => 'id'])
            ->where(['is_history' => 1])
            ->orderBy('created_at DESC');
    }
}
