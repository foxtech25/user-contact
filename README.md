git clone

composer install

php yii migrate

npm install

Genarate documentation:  $(npm bin)/apidoc -f .php -i controllers -o web/documentation

Run test:  php vendor/bin/codecept run
